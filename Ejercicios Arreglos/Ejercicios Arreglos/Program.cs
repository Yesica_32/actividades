﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_Arreglos
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                int Op;
                do
                {
                    Console.WriteLine("Elija que ejercicio desea hacer ");
                    Console.WriteLine("1-. Numeros entros enteros entre 4 y 14 ");
                    Console.WriteLine("2-. Numeros pares entre el 1 y 100 ");
                    Console.WriteLine("3-. Numero comprimidos entre 1 y 100 divicibles entre 3  ");
                    Console.WriteLine("4-. 10 Numeros aleatorios comprendidos entre 50 y 100 y su multiplicacion por 0.5"); 
                        Op = Convert.ToInt32(Console.ReadLine());
                    switch (Op)
                    {
                        case 1:
                            Arreglo1.Ejercicio1(); 
                            break;
                        case 2:
                            Arreglo1.Ejercicio2();
                            break;
                        case 3:
                            Arreglo1.Ejercicio3();
                            break;
                        case 4:
                            Arreglo1.Ejercicio4();
                            break;

                        default:
                            Console.WriteLine("Opcion no existente");
                            break;
                    }
                } while (Op != 1 && Op != 2 && Op != 3 && Op != 4);


                Console.ReadKey();

            }
        }
    }
}
