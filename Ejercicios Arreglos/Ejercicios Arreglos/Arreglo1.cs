﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_Arreglos
{
    class Arreglo1
    {
        public static void Ejercicio1()
        {
            int hasta = 14;

            Console.WriteLine("Numeros enteros entre 4 y 14");
            int[] array = new int[hasta];
            for (int i = 5; i < hasta; i++)
            {
                array[i] = i;
                Console.WriteLine(array[i]);
            }
            Console.ReadKey();
        }
        public static void Ejercicio2()
        {
            int hasta = 102;

            Console.WriteLine("Numeros pares entre  1 y  100");
            int[] array = new int[hasta];
            for (int i = 0; i < hasta; i = i + 2)
            {
                array[i] = i;
                Console.WriteLine(array[i]);
            }
            Console.ReadKey();
        }
        public static void Ejercicio3()
        {
            int hasta = 34;

            Console.WriteLine("Numeros pares entre  1 y  100");
            int[] array = new int[hasta];
            for (int i = 0; i < hasta; i ++)
            {
                array[i] = i*3;

              
                Console.WriteLine(array[i]);
            }
            Console.ReadKey();

        }
        public static void Ejercicio4()
        {
           Console.WriteLine("10 Numeros aleatorios comprendidos entre 50 y 100 y su multiplicacion por 0.5");
            int[] array = new int[10];
            int[] arraymult = new int[10];
            Random aleatorio = new Random();
            for (int i = 0; i < 10; i++)
            {
                int a = aleatorio.Next(50, 100);
                array[i] = i ;
                arraymult[i] = (a * 0.5);
                
                Console.WriteLine(array[i]+ "   "  + arraymult [i]);
            }
            Console.ReadKey();

        }
    }
}
