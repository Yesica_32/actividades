﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrices
{
    class M1
    {
        public static void Ejercicio1()
        {
            string[,] matriz = new string[4, 2];
            int[] sueldo = new int[4];
            int col = 0;
            int AM = 0; ///Sueldo mayor
            string EmpleadoMS = ""; ///Empleado con sueldo mayor 
            Console.WriteLine("Ingrese la informacion solicitada de los 4 empleados");
            for (int fila = 0; fila < 4; fila++)
            {
                for ( col = 0; col < 2; col++)
                {
                    if (col == 0)
                    {
                        Console.WriteLine("Introduce el nombre del empleado");
                        matriz[fila, col] = Console.ReadLine();
                    }
                    else
                    {
                        Console.WriteLine("Introduce el sueldo mensual del empleado");

                        matriz[fila, col] = Console.ReadLine();
                        sueldo[fila] = Convert.ToInt32(matriz[fila, col]) * 3;
                    }
                }
            }
            Console.Clear();
            Console.WriteLine("Tabla con nobre y suledo mensual de de cada empleado\n Y Sueldo trimestral ");
            for (int fila = 0; fila < 4; fila++)
            {
                for (col = 0; col < 2; col++)
                {
                    Console.SetCursorPosition(fila * 10, col + 1);
                    Console.WriteLine(matriz[fila, col]);
                }
                Console.SetCursorPosition(fila * 10, col + 2);
                Console.Write(sueldo[fila]);
                if (sueldo[fila] > AM)
                {
                    AM = sueldo[fila];
                    EmpleadoMS = matriz[fila, 0];
                }
            }
            Console.WriteLine("\n\nEl Empleado con mayor sueldo  es " + EmpleadoMS +
            " con un sueldo trimestral de: " + AM);
            Console.ReadKey();
        }
        public static void Ejercicio2()
        {
            int fil = 0; int col = 0; int T = 0; int MI = 0; string linea;
            Console.Write("Tamaño de la letra ");
            linea = Console.ReadLine();
            T = int.Parse(linea);
            T = (T % 2 == 0 ? T + 1 : T);
            string[,] MAT = new string[T + 1, T + 1];
            for (fil = 1; fil <= T; fil++)
            {
                for (col = 1; col <= T; col++)
                {
                    MAT[fil, col] = " ";
                }
            }
            MI = T / 2 + 1;
            for (fil = 1; fil <= T; fil++)
            {
                MAT[fil, 1] = "L"; ///Y,X
                MAT[fil / 2, T] = "M";
                MAT[1, fil] = "N";
                MAT[MI, fil] = "O";
                for (int q = MI; q < fil; q++)
                {
                    MAT[fil, fil] = "P";
                }
            }
            for (fil = 1; fil <= T; fil++)

            {
                for (col = 1; col <= T; col++)
                {
                    Console.SetCursorPosition(col, fil + 1);
                    Console.Write(MAT[fil, col]);
                }
            }
            Console.ReadKey();
        }
        public static void Ejercicio3()
        {
            Console.WriteLine("Tamaño de matriz (en X)");
            int Matriz1 = Convert.ToInt32(Console.ReadLine());
            int[,] matriz2 = new int[Matriz1, Matriz1];
            int NumeroMayor = 0;
            int NumeroMenor = 0;

            Console.WriteLine("Introduce los números de la matriz");
            for (int fila = 0; fila < Matriz1; fila++)
            {
                for (int colu = 0; colu < Matriz1; colu++)
                {
                    Console.SetCursorPosition(2 + (fila * 2), 4 + (colu * 2));
                    matriz2[fila, colu] = Convert.ToInt32(Console.ReadLine());
                    Console.SetCursorPosition(2 + (fila * 2), 4 + (colu * 2));
                    Console.WriteLine(matriz2[fila, colu]);
                    if (matriz2[fila, colu] >= NumeroMayor)
                    {
                        NumeroMayor = matriz2[fila, colu];
                    }
                    if (matriz2[fila, colu] < NumeroMenor)
                    {
                        NumeroMenor = matriz2[fila, colu];
                    }
                }
            }
            NumeroMenor = NumeroMayor;

            for (int fil = 0; fil < Matriz1; fil++)
            {
                for (int col = 0; col < Matriz1; col++)
                {
                    if (matriz2[fil, col] < NumeroMenor)
                    {
                        NumeroMenor = matriz2[fil, col];
                    }
                }
            }
            Console.WriteLine("Numero mayor de la matriz es " + NumeroMayor + ".\nNumero menor de la matriz es " + NumeroMenor + ".");
            Console.ReadKey();
        }
        public static void Ejercicio4()
        {
            Console.WriteLine("Tamaño de matriz (en X)");
            int m = Convert.ToInt32(Console.ReadLine());
            int[,] matriz = new int[m, m];
            int[,] matrizOrd = new int[m, m];
            int mayor = 0;
            int menor = 0;
            int x = 0;
            int y = 0;
            Console.WriteLine("Introduce los números de la matriz");
            for (int i = 0; i < m; i++) ///for usado para llenar el matriz
            {
                for (int c = 0; c < m; c++)
                {
                    Console.SetCursorPosition(2 + (i * 3), 4 + (c * 2));
                    matriz[i, c] = Convert.ToInt32(Console.ReadLine());
                    Console.SetCursorPosition(2 + (i * 3), 4 + (c * 2));
                    Console.WriteLine(matriz[i, c]);
                    if (matriz[i, c] >= mayor) ///se consigue el número mayor de la matriz
                    {
                        mayor = matriz[i, c];
                    }
                }
            }

            for (int p = 0; p < m; p++)///for anidado cuádruple, ajua. Primeros dos for para llenar la matriz con los números ordenados
            {
                for (int f = 0; f < m; f++)
                {
                    menor = mayor;
                    for (int i = 0; i < m; i++)///Dos últmos for para recorrer la primera matriz desordenada
                    {
                        for (int c = 0; c < m; c++)
                        {
                            if (matriz[i, c] < menor)///se encuentra el valor menor de la matriz desordenada
                            {
                                menor = matriz[i, c];
                                x = i;///almacenamos la posición de el valor menor
                                y = c;
                            }
                        }
                    }
                    matrizOrd[p, f] = menor;///almacenamos en la matriz el primer valor menor
                    matriz[x, y] = mayor;///modificamos el valor menor para que deje de ser el valor menor y pase con el siguiente
                }
            }
            for (int i = 0; i < m; i++)///imprimimos la matriz ordenada
            {
                for (int c = 0; c < m; c++)

                {
                    Console.SetCursorPosition(2 + (i * 3), 5 + (m * 2) + (c * 2));
                    Console.WriteLine(matrizOrd[i, c]);
                }
            }
            Console.ReadKey();
        }
        public static void Ejercicio5()
        {
            Console.WriteLine("Introduce el tamaño de la matriz en x");
            int m = Convert.ToInt32(Console.ReadLine());
            int[,] matriz = new int[m, m];
            int mayor = 0;
            int menor = 0;
            Console.WriteLine("Introduce los números de la matriz");
            for (int i = 0; i < m; i++) ///for usado para llenar el matriz
            {
                for (int c = 0; c < m; c++)
                {
                    Console.SetCursorPosition(2 + (i * 3), 4 + (c * 2));
                    matriz[i, c] = Convert.ToInt32(Console.ReadLine());
                    Console.SetCursorPosition(2 + (i * 3), 4 + (c * 2));
                    Console.WriteLine(matriz[i, c]);
                    if (matriz[i, c] >= mayor) ///se consigue el número mayor de la matriz
                    {
                        mayor = matriz[i, c];
                    }
                }
            }
            for (int i = 0; i < m; i++) ///for usado para llenar el matriz
            {
                menor = mayor;
                for (int c = 0; c < m; c++)
                {

                    if (matriz[i, c] < menor)///se encuentra el valor menor de la matriz desordenada
                    {
                        menor = matriz[i, c];
                    }
                }
                Console.SetCursorPosition(2 + (i * 3), 5 + (m * 2));
                Console.WriteLine(menor);
            }
            Console.ReadKey();
        }
        public static void Ejercicio6()
        {
            Console.WriteLine("Introduce el tamaño de la matriz en x");
            int m = Convert.ToInt32(Console.ReadLine());
            int[,] matriz = new int[m, m];
            int mayor = 0;
            Console.WriteLine("Introduce los números de la matriz");
            for (int i = 0; i < m; i++) ///for usado para llenar el matriz
            {
                for (int c = 0; c < m; c++)
                {
                    Console.SetCursorPosition(2 + (i * 3), 4 + (c * 2));
                    matriz[i, c] = Convert.ToInt32(Console.ReadLine());
                    Console.SetCursorPosition(2 + (i * 3), 4 + (c * 2));
                    Console.WriteLine(matriz[i, c]);
                }
            }
            for (int c = 0; c < m; c++) ///for usado para llenar el matriz
            {
                mayor = 0;
                for (int i = 0; i < m; i++)
                {
                    if (matriz[i, c] >= mayor) ///se consigue el número mayor de la matriz
                    {
                        mayor = matriz[i, c];
                    }

                }
                Console.SetCursorPosition((m * 4), 4 + (c * 2));
                Console.WriteLine(mayor);
            }
            Console.WriteLine("Presione una tecla para regresar");
            Console.ReadKey();
        }



    }
}
