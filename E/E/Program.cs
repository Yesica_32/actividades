﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AyudaMatrices
{
    class Program
    {
        /// <summary>
        /// Este es el main 
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.WriteLine("1)          2)");
            int a = int.Parse(Console.ReadLine());
            switch (a)
            {
                case 1:
                    E.Herramientas.reservaciones();                   
                    break;
                case 2:
                    E.Herramientas.matriz();
                    break;
            }
        }
    }
}
