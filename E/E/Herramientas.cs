﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E
{
    class Herramientas
    {
        public static void reservaciones()
        {
            Console.Clear();
            int a, b, n = 0, f, contador = 0, contador2 = 0;
            int[] asientos = new int[10];
            int[] total = new int[10];
            int[] pase = new int[10];
            for (f = 0; f < 10; f++)
            {
                asientos[f] = 0;
                total[f] = f + 1;
            }
            do
            {
                Console.WriteLine("Seleciona una opción");
                Console.WriteLine("1) Primera clase     2) Economico");
                a = int.Parse(Console.ReadLine());
                switch (a)
                {
                    case 1:
                        Console.WriteLine("\nPrimera clase - Asientos 1 - 5");
                        Console.WriteLine("\nAsientos desocupados");
                        Console.WriteLine(5 - contador);
                        for (f = 0; f < 5; f++)
                        {
                            Console.SetCursorPosition(f * 6, 8);
                            Console.WriteLine(total[f]);
                            Console.SetCursorPosition(f * 6, 9);
                            Console.WriteLine(asientos[f]);
                        }
                        if (contador == 5)
                        {
                            Console.WriteLine("No hay asientos disponibles");
                        }
                        else
                        {
                            do
                            {
                                Console.WriteLine("\n¿Cuántos asientos deseas reservar?");
                                n = int.Parse(Console.ReadLine());
                                if (5 - contador < n)
                                {
                                    Console.WriteLine("No hay tantos asientos disponibles");
                                    Console.WriteLine("Selecciona otra cantidad");
                                }

                            } while (5 - contador < n);
                            for (f = 0; f < n; f++)
                            {
                                do
                                {
                                    Console.WriteLine("\nIngresa el asiento deseado");
                                    b = int.Parse(Console.ReadLine());
                                    if (b > 5)
                                    {
                                        Console.WriteLine("Pertenece a la sección economica, selecciona otro");
                                    }
                                    else if (asientos[b - 1] == 1)
                                    {
                                        Console.WriteLine("Asiento ocupado, selecciona otro");
                                    }

                                } while (b > 5 || asientos[b - 1] == 1);
                                asientos[b - 1] = 1;
                                pase[f] = b;
                                contador++;
                            }
                            Console.Clear();
                            Console.WriteLine("\nPase de abordar");
                            Console.WriteLine("Asientos primera clase");
                            Console.WriteLine("\n");
                            for (f = 0; f < n; f++)
                            {
                                Console.SetCursorPosition(f * 6, 3);
                                Console.WriteLine(pase[f]);
                            }
                        }

                        break;

                    case 2:
                        Console.WriteLine("\nEconomico - Asientos 6 - 10");
                        Console.WriteLine("\nAsientos desocupados");
                        Console.WriteLine(5 - contador2);
                        for (f = 5; f < 10; f++)
                        {
                            Console.SetCursorPosition((f - 5) * 6, 8);
                            Console.WriteLine(total[f]);
                            Console.SetCursorPosition((f - 5) * 6, 9);
                            Console.WriteLine(asientos[f]);
                        }
                        if (contador2 == 5)
                        {
                            Console.WriteLine("No hay asientos disponibles");
                        }
                        else
                        {
                            do
                            {
                                Console.WriteLine("\n¿Cuántos asientos deseas reservar?");
                                n = int.Parse(Console.ReadLine());
                                if (5 - contador2 < n)
                                {
                                    Console.WriteLine("No hay tantos asientos disponibles");
                                    Console.WriteLine("Selecciona otra cantidad");
                                }
                            } while (5 - contador2 < n);
                            if (contador2 == 5)
                            {
                                do
                                {
                                    Console.WriteLine("¿Reservar más asientos en clase economica?");
                                    Console.WriteLine("1) Si   2) No");
                                    n = int.Parse(Console.ReadLine());
                                    if (n != 1 && n != 2)
                                    {
                                        Console.WriteLine("Ingresa una opción correcta");
                                    }
                                } while (n != 1 && n != 2);

                            }
                            else
                            {
                                for (f = 0; f < n; f++)
                                {
                                    do
                                    {
                                        Console.WriteLine("\nIngresa el asiento deseado");
                                        b = int.Parse(Console.ReadLine());
                                        if (b <= 5)
                                        {
                                            Console.WriteLine("Pertenece a la primera clase, selecciona otro");
                                        }
                                        else if (asientos[b - 1] == 1)
                                        {
                                            Console.WriteLine("Asiento ocupado, selecciona otro");
                                        }

                                    } while (b <= 5 || asientos[b - 1] == 1);
                                    asientos[b - 1] = 1;
                                    pase[f] = b;
                                    contador2++;
                                }
                                Console.Clear();
                                Console.WriteLine("\nPase de abordar");
                                Console.WriteLine("Asientos economicos");
                                Console.WriteLine("\n");
                                for (f = 0; f < n; f++)
                                {
                                    Console.SetCursorPosition(f * 6, 3);
                                    Console.WriteLine(pase[f]);
                                }
                            }
                        }

                        break;
                }
                do
                {
                    Console.WriteLine("\n¿Reservar más asientos?");
                    Console.WriteLine("1) Si   2) No");
                    n = int.Parse(Console.ReadLine());
                    if (n != 1 && n != 2)
                    {
                        Console.WriteLine("Ingresa una opción correcta");
                    }
                } while (n != 1 && n != 2);
                Console.Clear();
            } while (n == 1);
        }

        public static void matriz()
        {
            int f, c, contador = 0;
            double suma = 0, comparador = 0;
            int[] mayor = new int[5];
            string[,] tabla = new string[6, 6];
            double[,] votos = new double[7, 6];
            tabla[0, 0] = "Distrito";
            tabla[1, 1] = "A";
            votos[0, 1] = 194;
            votos[1, 1] = 180;
            votos[2, 1] = 221;
            votos[3, 1] = 432;
            votos[4, 1] = 820;
              for (f = 0; f < 4; f++)
              {
                suma = suma + votos[f, 1];
              }
            votos[5, 1] = suma;
            suma = 0;
            tabla[1, 2] = "B";
            votos[0, 2] = 48;
            votos[1, 2] = 20;
            votos[2, 2] = 90;
            votos[3, 2] = 50;
            votos[4, 2] = 61;
              for (f = 0; f < 4; f++)
              {
                suma = suma + votos[f, 2];
              }
            votos[5, 2] = suma;
            suma = 0;
            tabla[1, 3] = "C";
            votos[0, 3] = 206;
            votos[1, 3] = 320;
            votos[2, 3] = 140;
            votos[3, 3] = 821;
            votos[4, 3] = 946;
              for (f = 0; f < 4; f++)
              {
                suma = suma + votos[f, 3];
              }
            votos[5, 3] = suma;
            suma = 0;
            tabla[1, 4] = "D";
            votos[0, 4] = 45;
            votos[1, 4] = 16;
            votos[2, 4] = 20;
            votos[3, 4] = 14;
            votos[4, 4] = 18;
             for (f = 0; f < 4; f++)
             {
                suma = suma + votos[f, 4];
             }
            votos[5, 4] = suma;
            suma = 0;
             for (f = 0; f < 5; f++)
             {
                  for (c = 0; c < 5; c++)
                  {
                    tabla[0, f + 1] = "Candidato";
                    Console.SetCursorPosition(f * 12, c);
                    Console.WriteLine(tabla[c, f]);
                    Console.SetCursorPosition(f * 12, c + 2);
                    Console.WriteLine(votos[c, f]);
                    votos[f, 0] = f + 1;
                    Console.SetCursorPosition(0, f + 2);
                    Console.WriteLine(votos[f, 0]);

                  }
             }
            Console.WriteLine("\nTotal de votos");
              for (c = 1; c < 5; c++)
              {
                Console.SetCursorPosition(c * 12, f + 4);
                Console.WriteLine(votos[5, c]);
                suma = suma + votos[5, c];
             }
              for (c = 1; c < 4; c++)
              {
                 if (votos[5, c] > comparador)
                 {
                    contador++;
                    comparador = votos[5, c];
                    mayor[contador] = c;

                 }
              }
            comparador = 0;
            Console.WriteLine("\nEl candidato con mas votos es el: " + tabla[1, mayor[contador]]);
             for (c = 1; c < 4; c++)
             {
                votos[6, c] = (votos[5, c] / suma) * 100;

                 if (votos[6, c] > 50)
                 {
                    comparador = 1;
                 }
             }
             if (comparador == 1)
             {
                Console.WriteLine("El candidato " + tabla[1, mayor[contador]] + " es el ganador con: " + Math.Round(votos[6, mayor[contador]]) + "% de los votos");
             }
             else
             {
                Console.WriteLine("El candidato " + tabla[1, mayor[contador]] + " y el candidato " + tabla[1, mayor[contador - 1]] + " pasaron a la segunda ronda de elecciones");
             }
            Console.ReadKey();
        }
    }
}
   

