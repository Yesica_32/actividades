﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace A_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int Op;
            do
            {
                Console.WriteLine("Elija que programa decea ejecutar");
                Console.WriteLine("1-. 100 primeros numeros enteros de forma acendente ");
                Console.WriteLine("2-. 100 primeros numeros enteros de forma desendente ");
                Console.WriteLine("3-. En construccion ");
                Op = Convert.ToInt32(Console.ReadLine());
                switch (Op)
                {
                    case 1:
                        Ejercicios .E1(); ///1.-  Que rellene un array con los 100 primeros números enteros y los muestre en pantalla en orden ascendente.
                        break;
                    case 2:
                        Ejercicios.E2();///2.- Que rellene un array con los 100 primeros números enteros y los muestre en pantalla en orden descendente.
                        break;
                    case 3:
                        Ejercicios.E3();  /// 3.- Que lea 10 números por teclado, los almacene en un array y muestre la suma, resta, multiplicación y división de todos.
                        break;
                    default:
                        Console.WriteLine("Opcion no existente");
                        break;
                }
            } while (Op != 1 && Op != 2 && Op != 3 && Op != 4);


            Console.ReadKey();

        }
    }
}
