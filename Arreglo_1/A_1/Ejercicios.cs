﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_Arreglos
{
    class Arreglo1
    {
        public static void Ejercicio9()
        {
            string a;
            string b;
            Console.WriteLine("Ingrese frase");
            a = Console.ReadLine();
            Console.WriteLine("Ingrese frase");
            b = Console.ReadLine();
            if (a.Length == b.Length)
            {
                for (int i = 0; i < a.Length; i++)
                {
                    if (a[i] == b[i])
                    {
                        Console.WriteLine("Son iguales");
                    }
                    else
                    {
                        Console.WriteLine("No son iguales");
                    }
                }
            }
            else
            {
                Console.WriteLine("Las palabras o frases ingresadas no son iguales");
            }
        }
    }
}