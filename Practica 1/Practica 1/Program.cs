﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int Op;
            do
            {
                Console.WriteLine("Elija que programa decea ejecutar");
                Console.WriteLine("1-. Programa inserte nuemros mayores  que numero inicial");
                Console.WriteLine("2-. Tablas de multiplicar de la 2 a la 10 ");
                Console.WriteLine("3-. Usuario y su contraseña  ( código 1024 y como contraseña 4567) ");
                Op = Convert.ToInt32(Console.ReadLine());
                switch (Op)
                {
                    case 1:
                        Practica.E1(); /// -Escribe un programa que pida números decimales mientras que el usuario escriba números mayores que el primero
                        break;
                    case 2:
                        Practica.E2();///Escribir un programa que haga las tablas de multiplicar de la 2 a la 10 
                        break;
                    case 3:
                        Practica.E3();  ///Crear un programa que pida al usuario su código de usuario y su contraseña numérica y no le permita seguir hasta que introduzca como código 1024 y como contraseña 4567
                        break;
                    default:
                        Console.WriteLine("Opcion no existente");
                        break;
                }
            } while (Op != 1  && Op != 2 && Op != 3 && Op != 4 );


            Console.ReadKey();

        }

    }
}
